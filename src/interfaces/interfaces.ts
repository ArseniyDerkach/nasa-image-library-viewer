interface SearchResultData {
  title: string,
  nasa_id: string,
  description: string,
  photographer?: string
}

export interface SearchResultInterface {
  data: Array<SearchResultData>,
  links: Array<{
    href: string
  }>
}

export interface ImageDataInterface {
  collection: {
    href: string,
    items: Array<{
      href: string
    }>
  }
}