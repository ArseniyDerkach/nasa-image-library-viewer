import Box from "@mui/material/Box";

export function NotFound() {
  return (
    <Box sx={{
      paddingTop: "20px",
      textAlign: "center"
    }}>
      No results, please try again
    </Box>
  )
}