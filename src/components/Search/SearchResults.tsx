import { SearchResultInterface } from "../../interfaces/interfaces";
import SearchResult from "./SearchResult";
import Box from "@mui/material/Box";
import Masonry from "@mui/lab/Masonry";
import Link from "@mui/material/Link";

interface SearchResultsProps  {
  collection: {
    items: Array<SearchResultInterface>,
    links: Array<{ href: string, prompt: string }>,
  },
  handlePagination: (url: string) => void
}

export default function SearchResults(props: SearchResultsProps) {
  const { collection, handlePagination } = props;
  return (
    <Box sx={{
      paddingBottom: "20px"
    }}>
      <Masonry columns={{ xs: 1, sm: 2, md: 3, lg: 4 }} spacing={2}>
        {collection?.items && collection?.items.map(item => (
          <div className="search-results__item" key={item.data[0].nasa_id}>
            <SearchResult searchResult={item} />
          </div>
        ))}
      </Masonry>
      <Box sx={{
        display: "flex",
        justifyContent: "center",
        gap: "5px"
      }}>
        {collection?.links && collection?.links.length && collection.links.map(link => (
          <Link
            key={link.href}
            data-testid="navigation"
            component="button"
            underline="none"
            onClick={() => handlePagination(link.href)}
          >{link.prompt}</Link>
        ))}
      </Box>
    </Box>
  )
}