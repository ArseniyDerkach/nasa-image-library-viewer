import { fireEvent, render } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import Search from "./Search";
// import SearchResults from "./SearchResults";
import SearchResult from "./SearchResult";
import "@testing-library/jest-dom";
import { Provider } from "react-redux";
import { Action, Dispatch } from "redux";
import configureStore, { MockStore } from "redux-mock-store";
import { SearchResultInterface } from "../../interfaces/interfaces";

interface RootState {
  search: {
    searchValue: string;
    dateRange: string[];
    page: string;
  };
}

const mockStore = configureStore<RootState, Dispatch<Action<string>>>();


describe("Search", () => {
  let store: MockStore;
  let mockHandleSearch: jest.Mock;

  beforeEach(() => {
    mockHandleSearch = jest.fn();

    const initialState: RootState = {
      search: {
        searchValue: "",
        dateRange: ["", ""],
        page: "1",
      },
    };

    store = mockStore(initialState);
  });

  afterEach(() => {
    mockHandleSearch.mockClear();
  });

  test("should render", () => {
    render(
      <Provider store={store}>
        <Search handleSearch={mockHandleSearch} />
      </Provider>
      );
    expect(true).toBe(true);
  })
  test("should not search while empty input", () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <Search handleSearch={mockHandleSearch} />
      </Provider>
    )
    const searchForm = getByTestId("search-form");

    fireEvent.submit(searchForm);

    expect(mockHandleSearch).not.toHaveBeenCalled();
  })

  test("should trigger search", () => {

    const { getByTestId } = render(
      <Provider store={store}>
        <Search handleSearch={mockHandleSearch} />
      </Provider>
    )

    const searchInput = getByTestId("search-input");

    fireEvent.change(searchInput, { target: { value: "Apollo 11" } });

    const searchForm = getByTestId("search-form");

    fireEvent.submit(searchForm);

    expect(mockHandleSearch).toHaveBeenCalled();
  })
})


describe("SearchResult", () => {
  let mockSearchResult: SearchResultInterface;
  beforeEach(() => {
    mockSearchResult =  {
      data: [
        {
          title: "Apollo 11 50th Anniversary Celebration",
          photographer: "NASA/Connie Moore",
          nasa_id: "NHQ201907180120",
          description: "The Moon to Mars exhibit is seen at the Apollo 11 50th Anniversary celebration on the National Mall, Thursday, July 18, 2019 in Washington. Apollo 11 was the first mission to land astronauts on the Moon and launched on July 16, 1969 with astronauts Neil Armstrong, Michael Collins, and Buzz Aldrin. Photo Credit: (NASA/Connie Moore)"
        }
      ],
      links: [
        {
          href: "https://images-assets.nasa.gov/image/NHQ201907180120/NHQ201907180120~thumb.jpg"
        }
      ]
    }
  })
  test("should show image", () => {
    mockSearchResult
    const { getByRole } = render(
      <Router>
        <SearchResult searchResult={mockSearchResult} />
      </Router>
    )
    const imgElement = getByRole("img");

    // const title = getByText("Apollo 11 50th Anniversary Celebration");
    
    expect(imgElement).toBeInTheDocument();
  }) 

  test("should show title", () => {
    mockSearchResult
    const { getByText } = render(
      <Router>
        <SearchResult searchResult={mockSearchResult} />
      </Router>
    )

    const title = getByText("Apollo 11 50th Anniversary Celebration");
    
    expect(title).toBeInTheDocument();
  }) 
})