import { useState } from "react";
import { useSelector } from "react-redux";
import type { RootState } from "../../redux/store";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Select, { SelectChangeEvent } from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";

type SearchProps = {
  handleSearch: (searchValue: string, range: [string, string]) => void
}

export default function Search(props: SearchProps) {

  const { handleSearch } = props;
  const { searchValue, dateRange } = useSelector((state: RootState)=> state.search)
  const [value, setValue] = useState<string>(searchValue);
  const [range, setRange] = useState<[string, string]>(dateRange?.length ? dateRange : ["",""]);
  const searchPattern = "[a-zA-Z0-9\\s]*";

  const currentYear = new Date().getFullYear();
  const startYear = 1958; // year NASA has been created
  const years = Array.from({ length: currentYear - startYear + 1 }, (_, index) => startYear + index);


  function setDateFrom(value: string) {
    setRange((prev: [string, string]) => {
      return [
        value !== "Not set" ? value : "",
        prev[1]
      ]
    })
  }

  function setDateTo(value: string) {
    setRange((prev: [string, string]) => {
      return [
        prev[0],
        value !== "Not set" ? value : "",
      ]
    })
  }


  return (
    <Box
      component="form"
      data-testid="search-form"
      onSubmit={(e) => {
        if (!value) return;
        e.preventDefault();
        handleSearch(value, range)
      }}
      sx={{
        position: "relative",
        maxWidth: "400px",
        margin: "0 auto 20px",
        padding: "10px"
      }}
    >
      <TextField
        placeholder="search media collections"
        value={value}
        onChange={(e) => setValue(e.currentTarget.value)}
        variant="outlined"
        type="search"
        fullWidth
        required
        inputProps={{
          pattern: searchPattern,
          "data-testid": "search-input",
        }}
        sx={{
          width: "100%",
          maxWidth: "400px",
          "& input": {
            height: "20px",
            paddingRight: "80px"
          },
        }}
      />
      <Box sx={{
        display: "flex",
        gap: "5px",
        marginTop: "10px",
      }}>
        <FormControl sx={{
          flex: "1 1 50%"
        }}>
          <InputLabel id="year-select-from-label">Year from</InputLabel>
          <Select
            labelId="year-select-from-label"
            value={range[0] || "Not set"}
            onChange={(e: SelectChangeEvent<string>) => setDateFrom(e.target.value)}
            label="Year from"
          >
            <MenuItem value={"Not set"}>Not set</MenuItem>
          {years.map((year) => (
            <MenuItem key={year} value={year}>
              {year}
            </MenuItem>
          ))}
          </Select>
        </FormControl>
        <FormControl sx={{
          flex: "1 1 50%"
        }}>
          <InputLabel id="year-select-to-label">Year to</InputLabel>
          <Select
            labelId="year-select-to-label"
            value={range[1] || "Not set"}
            onChange={(e: SelectChangeEvent<string>) => setDateTo(e.target.value)}
            label="Year to"
            >
              <MenuItem value={"Not set"}>Not set</MenuItem>
            {years.map((year) => (
              <MenuItem key={year} value={year}>
                {year}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
      </Box>
      <Button
        type="submit"
        data-testid="submit-button"
        sx={{
          height: "53px",
          boxShadow: "none",
          position: "absolute",
          right: "10px",
          top: "10px"
        }}
      >search</Button>
    </Box>
  )
}