import { SearchResultInterface } from "../../interfaces/interfaces";
import { Link as RouterLink } from "react-router-dom";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";

type SearchResultProps = {
  searchResult: SearchResultInterface
}

export default function SearchResult(props: SearchResultProps) {
  const { searchResult: {
    data: [itemData],
    links: [itemLink]
  } } = props;
  return (
    <Box>
      <Link component={ RouterLink } to={`/show/${itemData.nasa_id}`}>
        <Box
          component="img"
          sx={{
            maxWidth: "100%"
          }}
          src={itemLink.href}
          alt={itemData.description}
        />
        <p>{itemData.title}</p>
        
      </Link>
        {itemData.photographer && (<Box component="p">By { itemData.photographer }</Box>)}
      </Box>
  )
}