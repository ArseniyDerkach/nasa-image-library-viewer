import { configureStore } from '@reduxjs/toolkit';
import { imagesApi } from './api.slice';
import searchSlice from './search.slice';

export const store = configureStore({
  reducer: {
    [searchSlice.name]: searchSlice.reducer,
    [imagesApi.reducerPath]: imagesApi.reducer
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(imagesApi.middleware)
})

export type RootState = ReturnType<typeof store.getState>;