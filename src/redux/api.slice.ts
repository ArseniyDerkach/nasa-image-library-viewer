import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const imagesApi = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({
    baseUrl: "https://images-api.nasa.gov/",
  }),
  endpoints: (builder) => ({
    search: builder.query({
      query: ({ searchValue, dateRange, page }) => {
        const dateRangeUrl = `${dateRange[0] ? `&year_start=${dateRange[0]}` : ''}${dateRange[1] ? `&year_end=${dateRange[1]}` : ''}`;
        return ({
          url: `/search?q=${searchValue}&media_type=image&page_size=20${dateRangeUrl}&page=${page}`
        })
      }
    }),
    getImage: builder.query({
      query: (nasa_id) => ({
        url: `/asset/${nasa_id}`
      })
    }),
    getImageData: builder.query({
      query: (nasa_id) => ({
        url: `/search?nasa_id=${nasa_id}`
      })
    })
  })
})

export const {
  useSearchQuery,
  useGetImageQuery,
  useGetImageDataQuery
} = imagesApi;