import type { PayloadAction } from "@reduxjs/toolkit";
import { createSlice } from "@reduxjs/toolkit";

interface SearchState {
  searchValue: string,
  dateRange: [string, string],
  page: string
}


export const searchSlice = createSlice({
  name: 'search',
  initialState: {searchValue: ''} as SearchState,
  reducers: {
    setSearch: (state, action: PayloadAction<SearchState>) => {
      state.searchValue = action.payload.searchValue;
      state.dateRange = action.payload.dateRange;
      state.page = action.payload.page;
    },
    setPage: (state, action: PayloadAction<{ page: string }>) => {
      state.page = action.payload.page
    }
  }
})

export const { setSearch, setPage } = searchSlice.actions;

export default searchSlice;