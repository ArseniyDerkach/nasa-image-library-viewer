import { SkipToken, skipToken } from "@reduxjs/toolkit/query/react";
import Search from "../../components/Search/Search";
import SearchResults from "../../components/Search/SearchResults";
import { useState } from "react";
import { useSearchQuery } from "../../redux/api.slice";
import { setSearch, setPage } from "../../redux/search.slice";
import { useDispatch, useSelector } from "react-redux";
import type { RootState } from "../../redux/store";
import Container from "@mui/material/Container";
import { NotFound } from "../../components/NotFound/NotFound";

interface searchQueryInterface {
  searchValue: string,
  dateRange: [string, string],
  page: string
}

export default function HomePage() {

  const dispatch = useDispatch();
  const { searchValue, dateRange, page } = useSelector((state: RootState) => state.search);
  const [searchQuery, setSearchQuery] = useState<searchQueryInterface | SkipToken>(searchValue ? {
    searchValue,
    dateRange,
    page
  } : skipToken);
  const { data } = useSearchQuery(searchQuery);
  
  function handleSearch(searchValue: string, range: [string, string]) {
    setSearchQuery({ searchValue, dateRange: range, page: "1" });
    dispatch(setSearch({ searchValue, dateRange: range, page: "1" }));
  } 

  function handlePagination(url: string) {
    const urlSearchParams = new URLSearchParams(url.split("?")[1]);
    const page = urlSearchParams.get("page") || "1";
    dispatch(setPage({ page }));
    setSearchQuery({ searchValue, dateRange, page });
  }

  if (!data?.collection?.items) return (
    <Container>
      <Search handleSearch={handleSearch} />
    </Container>
  )

  return (
    <Container>
      <Search handleSearch={handleSearch} />
      {data?.collection?.items?.length
        ?
          <SearchResults collection={data.collection} handlePagination={handlePagination} />
        :
          <NotFound />
      }
    </Container>
  )
}