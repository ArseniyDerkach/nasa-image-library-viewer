import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useGetImageQuery, useGetImageDataQuery } from "../../redux/api.slice";
import { useNavigate } from "react-router-dom";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";
import Container from "@mui/material/Container";
import CircularProgress from "@mui/material/CircularProgress";
import Typography from "@mui/material/Typography";

interface ImageLinksInterface {
  original: string,
  large?: string,
  medium?: string,
  small?: string
}
interface ItemDataInterface {
  description: string,
  keywords: Array<string>,
  photographer?: string,
  title: string,
  secondary_creator?: string,
  location?: string,
  date_created?: string
}

export default function ShowPage() {

  const { nasaId } = useParams();
  const navigate = useNavigate();

  const { data: imagesCollection } = useGetImageQuery(nasaId);
  const { data } = useGetImageDataQuery(nasaId) || {};
  const [imageLinks, setImageLinks] = useState<ImageLinksInterface>({
    original: "",
    large: "",
    medium: "",
    small: ""
  })


  useEffect(() => {
    const newImageLinks: ImageLinksInterface = {
      original: "",
      large: "",
      medium: "",
      small: ""
    }
    imagesCollection?.collection.items.forEach((item: {href: string}) => {
      switch (true) {
        case !!~item.href.indexOf("~orig"):
          newImageLinks.original = item.href.replace(/http:\/\//g, 'https://');
          break;
        case !!~item.href.indexOf("~large"):
          newImageLinks.large = item.href.replace(/http:\/\//g, 'https://');
          break;
        case !!~item.href.indexOf("~medium"):
          newImageLinks.medium = item.href.replace(/http:\/\//g, 'https://');
          break;
        case !!~item.href.indexOf("~small"):
          newImageLinks.small = item.href.replace(/http:\/\//g, 'https://');
          break;
      }
    })
    setImageLinks(newImageLinks);
  }, [imagesCollection]);
  
  const item = data?.collection?.items[0];
  const itemData: ItemDataInterface | undefined = item?.data[0];

  function parseDate(dateString: string) {
    const normalDate = new Date(dateString);
    return (
      <Box sx={{marginTop: "15px"}}>Photo taken on {normalDate.toLocaleDateString()}</Box>
    )
  }

  return (
    <main>
      <Container sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "flex-start",
        paddingBottom: "20px",
        minHeight: "100vh"
      }}>
        <Link
          component="button"
          underline="none"
          onClick={() => navigate(-1)}
          sx={{
            marginTop: "15px",
            fontSize: "1.4rem"
          }}
        >&larr; Go Back</Link>
        {itemData ? (
          <>
            <Typography variant="h5" sx={{
              marginTop: "15px"
            }}>{itemData.title}</Typography>
            {itemData.date_created && parseDate(itemData.date_created)}
            {(itemData?.photographer || itemData?.secondary_creator) &&
              <Box
                sx={{ marginTop: "15px" }}
              >
                By {itemData.photographer || itemData.secondary_creator}
              </Box>
            }
            <Box
              component="img"
              src={imageLinks.original}
              srcSet={(imageLinks.small && imageLinks.medium && imageLinks.large) ? `${imageLinks.small} 576w, ${imageLinks.medium} 991w, ${imageLinks.large} 1280w` : ""}
              style={{ maxWidth: "100%" }}
              alt={item?.data[0].description}
              sx={{
                alignSelf: "center",
                margin: "20px 0"
              }}
            ></Box>
            <p><b>Keywords:</b> {itemData.keywords.join(", ")}</p>
            {itemData.location && <p><b>Location:</b> {itemData.location}</p>}
            <p><b>Description:</b> {itemData.description}</p>
          </>
          ) :
          <CircularProgress sx={{
            position: "fixed",
            top: "calc(50% - 20px)",
            left: "calc(50% - 20px)"
          }} />
        }
      </Container>
    </main>
  )
}